import Starship from "./Starship";

export default class StarWarsUniverse {
    constructor() {
        this.starships = [];
    }
    get theBestStarship() {
        return this.starships.reduce((max, obj) => (max.maxDaysInSpace > obj.maxDaysInSpace) ? max : obj);
    }
    async init() {
        let count = await this._getStarshipCount();
        await this._createStarships(count);
    }
    async getapi(url) {
        try {
            const response = await fetch(url);
            const data = await response.json();
            return data;
        }
        catch (err) {
            return null;
        }
    }
    async _getStarshipCount() {
        let api_url = "https://swapi.boom.dev/api/starships/";
        let api_res = await this.getapi(api_url);
        return (api_res.count);
    }
    async _createStarships(count) {
        let api_url = "https://swapi.boom.dev/api/starships/";
        let _starships = []
        for (let i = 0; i <= count; i++) {
            let api_res = await this.getapi(api_url + i)
            if (api_res != null) {
                _starships.push({ id: i, data: api_res })
            }
        }
        for (let ship of _starships) {
            await this._validateData(ship.data);
        }
        for (let ship of this.starships) {
            let length, type;
            [length, type] = ship._consumables.split(' ')
            switch (type) {
                case 'days':
                    ship._consumables = Number(length);
                    break;
                case 'month':
                    ship._consumables = 30;
                    break;
                case 'months':
                    ship._consumables = length * 30;
                    break;
                case 'year':
                    ship._consumables = 365;
                    break;
                case 'years':
                    ship._consumables = length * 365;
                    break;
                default:
                    break;
            }
            ship._passengers = Number(ship._passengers.replace(',', ''));
        }
    }
    async _validateData(starship) {
        switch (starship.consumables) {
            case 'undefined':
                break;
            case 'null':
                break;
            case 'unknown':
                break;
            default:
                switch (starship.passengers) {
                    case 'undefined':
                        break;
                    case 'null':
                        break;
                    case 'n/a':
                        break;
                    case '0':
                        break;
                    default:
                        this.starships.push(new Starship(starship.name, starship.consumables, starship.passengers))
                        break;
                }
                break;
        }
    }
};